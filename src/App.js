import './App.css';
import Header from "./Components/Header";
import Banner from "./Components/Banner";
import Item from "./Components/Item";
import Footer from "./Components/Footer";

function App() {
  return (
    <div className="App">
        <Header/>
        <Banner/>
        <Item/>
        <Footer/>
    </div>
  );
}

export default App;
